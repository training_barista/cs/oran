﻿// See https://aka.ms/new-console-template for more information
class mainer { 
    public static bool ProperIntInput(String strToCheck, out int result)
    {
        // Gets a String, and checks if the string could be an int - while updating the int and returns whether the String works.
        bool canParse = int.TryParse(strToCheck, out result);
        return (canParse);
    }
    static void OptionHandler(int choice)
    {
        string input;
        int calcNum1 = 0;
        int calcNum2 = 0;
        //Handles the selection of functions
        switch (choice)
        {
            case 2:
                Targil2();
                break;
            case 3:
                Targil3();
                break;
            case 4:
                Targil4();
                break;
            case 5:
                int j = 0;
                int numbOut = 0;
                while (j < 2)
                {
                    Console.WriteLine("Enter an number: ");
                    input = Console.ReadLine();
                    if (j == 0)
                    {
                        if (ProperIntInput(input, out calcNum1))
                        {
                            j++;
                        }
                        else
                        {
                            Console.WriteLine("Not a valid number!");
                        }
                    }
                    else
                    {
                        if (ProperIntInput(input, out calcNum2))
                        {
                            j++;
                        }
                        else
                            Console.WriteLine("Not a valid number!");
                    }
                }
                Randomer(calcNum1, calcNum2, out numbOut);
                Console.WriteLine(numbOut);
                break;
            case 6:
                int i = 0;
                string operator1 = "";
                while (i != 3)
                {
                    if (i == 1)
                    {
                        Console.WriteLine("Enter an operator: ");
                        input = Console.ReadLine();
                        if (!(input == ("+") || input == ("-") ||
                            input == ("/") || input == ("*") || input == ("^")))
                        {
                            Console.WriteLine("Wrong Operator!");
                        }
                        else
                        {
                            i++;
                            operator1 = input;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Enter an number: ");
                        input = Console.ReadLine();
                        if (i == 0)
                        {
                            if (ProperIntInput(input, out calcNum1))
                            {
                                i++;
                            }
                            else
                            {
                                Console.WriteLine("Not a number!");
                            }
                        }
                        else
                        {
                            if (ProperIntInput(input, out calcNum2))
                            {
                                i++;
                            }
                        }
                    }

                }
                Calculator(calcNum1, calcNum2, operator1);
                break;

        }
    }
    public static void Targil2()
    {
        // Gets inputs until ' -1' is inputed 
        // Calculates all the correct inputs -and shows avg, highest, lowest, and # of fails (under 60)
        String input = "";
        int grade = 0;
        double num = 0.0;
        double sum = 0.0;
        double avg = 0.0;
        int losers = 0;
        int lowestG = 101;
        int highestG = -1;

        Console.WriteLine("Hello, please input grades, and input -1 when you are done:");

        while (grade != -1)
        {
            input = Console.ReadLine();
            if (ProperIntInput(input, out grade))
            {
                if (grade >= 0 && grade <= 100)
                {
                    sum += grade;
                    num++;
                    if (grade > highestG)
                        highestG = grade;
                    if (grade < lowestG)
                        lowestG = grade;
                    if (grade < 68)
                        losers++;
                }
                else
                    Console.WriteLine("Please enter a number between 1-100");
            }
            else
            {
                Console.WriteLine("Please enter a number");
            }
        }

        Console.WriteLine("Avg is:" + (sum / num) + ", Highest: " + highestG + ", Lowest: " + lowestG +
               ", # of fails: " + losers + ".");
    }

    public static void Targil3()
    {
        // prints all numbers 1-1000, except when the num
        // is dividable by 3, 5, or both, and prints "Fizz", "Buzz" or "Fizzbuzz" accordingly
        int i = 1;
        for (i = 1; i <= 1000; i++)
        {
            if (i % 5 == 0 && i % 3 == 0)
                Console.WriteLine("FizzBuzz");
            else if (i % 5 == 0)
                Console.WriteLine("Buzz");
            else if (i % 3 == 0)
                Console.WriteLine("Fizz");
            else
                Console.WriteLine(i);
        }
    }

    public static void Targil4()
    {
        // Gets a set of items - and checks if all of them divide by the first one
        String input = "";
        int number = 0;
        int divNum = 0;
        bool passedOnce = false;
        bool canPass = false;
        bool allDivable = true;
        Console.WriteLine("Please enter the set of numbers: (End by entering -1) (Negative numbers are allowed) ");
        while (number != -1)
        {
            input = Console.ReadLine();
            if (ProperIntInput(input, out number))
            {
                if (!passedOnce)
                {
                    divNum = number;
                    passedOnce = true;
                }
                else
                    if ((number != -1) && (number % divNum != 0))
                    allDivable = false;
            }
        }
        Console.WriteLine(allDivable);
    }
    public static void Randomer(int end1, int end2, out int x)
    {
        Random rnd = new Random();
        if (end1 < end2)
            x = rnd.Next(end1, end2);
        else
            x = rnd.Next(end2, end1);
    }

    public static void Calculator(int num1, int num2, string func)
    {
        switch (func)
        {
            case "+":
                Console.WriteLine(num1 + num2);
                break;
            case "-":
                Console.WriteLine(num1 - num2);
                break;
            case "*":
                Console.WriteLine(num1 * num2);
                break;
            case "/":
                Console.WriteLine(((double)num1) / ((double)num2));
                break;
            case "^":
                Console.WriteLine(Math.Pow((double)num1, (double)num2));
                break;          
        }
    }

    static void Main(string[] args)
    {
        Console.WriteLine("Choose what targil you want to see: (2-6)"); //Will update as I add more Targils
        int intChoice = 0;
        while (true)
        {
            String choice = Console.ReadLine();

            bool canParse = int.TryParse(choice, out intChoice);
            if (canParse && (intChoice >= 2 && intChoice <= 6))
            {  // Range of possible Nums
                OptionHandler(intChoice);
                Console.WriteLine("Choose what targil you want to see: (2-6)"); //Will update as I add more Targils
            }
            else
                Console.WriteLine("Please write a valid number - (currently 2-6):");
        }
    }
  }