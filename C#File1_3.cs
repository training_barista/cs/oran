﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    internal class Program
    {
        public static bool ProperIntInput(String strToCheck, out int result)
        {
            // Gets a String, and checks if the string could be an int - while updating the int and returns whether the String works.
            bool canParse = int.TryParse(strToCheck, out result);
            return (canParse);
        }
        static void OptionHandler(int choice)
        {
            //Handles the selection of functions
            switch (choice)
            {
                case 2:
                    Targil2();
                    break;
               case 3:
                    Targil3();
                    break;
               case 4:
                    Targil4();
                    break;
            }
        }
        public static void Targil2()
        {
            // Gets inputs until ' -1' is inputed 
            // Calculates all the correct inputs -and shows avg, highest, lowest, and # of fails (under 60)
            String input = "";
            int grade = 0;
            double num = 0.0;
            double sum = 0.0;
            double avg = 0.0;
            int losers = 0;
            int lowestG = 101;
            int highestG = -1;

            Console.WriteLine("Hello, please input grades, and input -1 when you are done:");

            while (grade != -1)
            {
                input = Console.ReadLine();           
                if (ProperIntInput(input,out grade))
                {
                    if (grade >= 0 && grade <= 100)
                    {
                        sum += grade;
                        num++;
                        if (grade > highestG)
                            highestG = grade;
                        if (grade < lowestG)
                            lowestG = grade;
                        if (grade < 68)
                            losers++;
                    }
                }
            }

            Console.WriteLine("Avg is:" + (sum / num) + ", Highest: " + highestG + ", Lowest: " + lowestG +
                   ", # of fails: " + losers + ".");
        }

        public static void Targil3()
        {
            // prints all numbers 1-1000, except when the num
            // is dividable by 3, 5, or both, and prints "Fizz", "Buzz" or "Fizzbuzz" accordingly
            int i = 1;
            for (i = 1; i <= 1000; i++)
            {
                if (i % 5 == 0 && i % 3 == 0)
                    Console.WriteLine("FizzBuzz");
                else if (i % 5 == 0)
                    Console.WriteLine("Buzz");
                else if (i % 3 == 0)
                    Console.WriteLine("Fizz");
                else
                    Console.WriteLine(i);
            }
        }

        public static void Targil4()
        {
            // Gets a set of items - and checks if all of them divide by the first one
            String input = "";
            int number = 0;
            int divNum = 0;
            bool passedOnce = false;
            bool canPass = false;
            bool allDivable = true;
            Console.WriteLine("Please enter the set of numbers: (End by entering -1) (Negative numbers are allowed) ");
            while (number != -1)
            {
                input = Console.ReadLine();
                if (ProperIntInput(input, out number))
                {
                    if (!passedOnce)
                    {
                        divNum = number;
                        passedOnce = true;
                    }
                    else
                        if ((number != -1) && (number % divNum != 0))
                        allDivable = false;
                }
            }
            Console.WriteLine(allDivable);     
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Choose what targil you want to see: (2-4)"); //Will update as I add more Targils
            int intChoice = 0;
            while (true)
            {
                String choice = Console.ReadLine();

                bool canParse = int.TryParse(choice, out intChoice);
                if (canParse && (intChoice >= 2 && intChoice <= 4))
                {  // Range of possible Nums
                    OptionHandler(intChoice);
                    Console.WriteLine("Choose what targil you want to see: (2-4)"); //Will update as I add more Targils
                }
                else
                    Console.WriteLine("Please write a valid number - (currently 2-4):");
            }
        }
    }
}
